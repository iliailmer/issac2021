\documentclass[11pt, aspectratio=169]{beamer}
%\usetheme[progressbar=frametitle]{metropolis}
%\usepackage[utf8]{inputenc}
%\usepackage[T1]{fontenc}
\usetheme{Arguelles}
\usepackage{textpos}
\usepackage{amsmath,amsthm,amsfonts}
\usepackage[hyperref=true,style=authoryear-icomp,doi=false,url=false,backend=biber,sorting=nyt,maxnames=20]{biblatex}

\usepackage[utf8]{inputenc}
\setbeamertemplate{footline}[page number]{}

\addbibresource{bibliography.bib}
\addtobeamertemplate{frametitle}{}{%
	\begin{textblock*}{\textwidth}(12.5cm, -0.65cm)
		\includegraphics[width=0.1\textwidth]{CUNY_Logo.png}~
		\includegraphics[width=0.05\textwidth]{NSF_4-Color_bitmap_Logo.png}
	\end{textblock*}}
\title{Maple Application for Structural Identifiability Analysis of ODE models}
\institute{\inst{1}Graduate Center CUNY \and \inst{2} CUNY Queens College \and \inst{3}Institute Polytechnqiue de Paris}
\author[Ilia Ilmer]{Ilia Ilmer\inst{1} \and Alexey Ovchinnikov\inst{2} \and Gleb Pogudin\inst{3}\hfill \includegraphics[width=.2\textwidth]{CUNY_Logo.png}~\includegraphics[width=.1\textwidth]{NSF_4-Color_bitmap_Logo.png}}
\date{July, 2021}

% \logo{
% 	\includegraphics[width=.15\textwidth]{CUNY_Logo.png}~
% 	\includegraphics[width=.08\textwidth]{NSF_4-Color_bitmap_Logo.png}
% }
% \titlegraphic{
% 	\includegraphics[width=.2\textwidth]{CUNY_Logo.png}~
% 	\includegraphics[width=.1\textwidth]{NSF_4-Color_bitmap_Logo.png}
% }
\begin{document}

\maketitle
\begin{frame}{Overview}
	\tableofcontents
\end{frame}
\section{Identifiability Problem and Examples}
\begin{frame}{Identifiability Problem}
	\begin{itemize}
		\item Ordinary differential equations (ODEs) are ubiquitous in modeling real-world phenomena\pause{}
		\item Quality of a model relies on identifiability of parameters \pause{}
		\item Two types of structural identifiability: \begin{itemize}
			      \item \emph{local} (finitely many values)
			      \item \emph{global} (uniquely recoverable from experimental data)
			      \item otherwise, a parameter is \emph{non-identifiable}.\pause{}
		      \end{itemize}
		\item Seek globally identifiable \emph{combinations} of parameters to mitigate the issue.
	\end{itemize}
\end{frame}
\begin{frame}[t]{Examples}
	\begin{example}[1]
		Consider a system \[ \Sigma:=\begin{cases}
				x'=Cx,  \\
				x(0)=K, \\
				y=x
			\end{cases}\]
		where \(C,K\in\mathbb{C}\) are constant parameters. \pause{}Then \(C,~K\) are globally identifiable:\pause{} \[K=y(0), ~C=\frac{y'(0)}{y(0)}.\]
	\end{example}
\end{frame}
\begin{frame}[t]{Examples}
	\begin{example}[1]
		Consider a system \[ \Sigma:=\begin{cases}
				x'=C^2, \\
				x(0)=K, \\
				y=x
			\end{cases}\]\pause{}
		Note, that \(y(t)=C^2t+K\).\pause{} Then \(C\) is locally identifiable: \[C^2=y(1)-y(0)\]while \(K=y(0)\) is globally identifiable.
	\end{example}
\end{frame}
\begin{frame}[t]{Examples}
	\begin{example}[2]
		Consider a system with 2 parameters \(C,K\) \[ \Sigma:=\begin{cases}
				x'=0, \\
				y_1=x,~y_2=Cx+K
			\end{cases}\]\pause{}
		Notice that none are identifiable: \(y_2=Cy_1+K.\)\pause{} However, for
		\[ \Sigma:=\begin{cases}
				x_{1,1}' = x_{2,1}' = 0,                 \\
				y_{1,1} = x_{1,1},~y_{1,2} = Cx_{1,1}+K, \\
				y_{2,1} = x_{2,1},~y_{2,2} = Cx_{2,1}+K,
			\end{cases}\]we can find  \(C=\frac{y_{2,2} - y_{1,2}}{y_{2,1}-y_{1,1}},~K=\frac{y_{1,1}y_{2,2} - y_{1,2}}{y_{2,1}y_{1,2}-y_{1,1}}\).
	\end{example}
\end{frame}
\section{Existing Algorithms}
\begin{frame}[t]{Existing Algorithms}
	There are several existing algorithms for individual parameter identifiability: \pause{}
	\begin{itemize}
		\item Series approaches, for instance GenSSI\footcite{chics2011genssi,ligon2018genssi}.\pause{}
		\item Differential algebra-based approaches  such as COMBOS\footcite{meshkat2014finding}, DAISY\footcite{saccomani2008daisy}.\pause{}
	\end{itemize}
	Several works study \emph{identifiable combinations} and multi-experiment identifiability, such as~\cite{joeiiibook} or~\cite{ovchinnikov2020computing}.
\end{frame}
\begin{frame}[t]{Existing Algorithms}
	\begin{itemize}
		% \item What should we use?\pause{}
		\item Difficult to keep track of the multitude of different approaches.\pause{}
		\item Some programs require non-trivial installation steps and are not available for all operating systems.\pause{}
		\item Others may depend on proprietary software (Matlab, {\sc Maple}).
	\end{itemize}
\end{frame}
\section{Structural Identifiability Toolbox}
\begin{frame}{Features\footnote{Our toolbox is available at \url{https://maple.cloud/app/6509768948056064/}}}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=0.5\textwidth]{frame.png}
			\end{figure}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{itemize}
				\item All-in-one approach to identifiability,  {\bf free} and {\bf needs no installation} \pause{}
				\item Implementation of algorithms from \cite{hong2019sian,ovchinnikov2020multi} provides a comprehensive identifiability tool.\pause{}
				\item The entire program is implemented in {\sc Maple} and is hosted on Maple Cloud website.
			\end{itemize}
		\end{column}
	\end{columns}
\end{frame}
\section{Toolbox Details}
%\subsection{Step 1: Individual parameter identifiability}
\begin{frame}[t]{Step 1: Individual parameter identifiability}
	\begin{itemize}
		\item Input: an ODE model \[\Sigma:=\begin{cases}
				      \mathbf{x}'=\mathbf{f}(\mathbf{x},\boldsymbol{\mu}, \mathbf{u}), \\
				      \mathbf{y}=\mathbf{g}(\mathbf{x}, \boldsymbol{\mu}, \mathbf{u}).
			      \end{cases}\]
		      \(\mathbf{f}=(f_1, \dots, f_n)\) and \(\mathbf{g}=(g_1,\dots,g_n)\) are tuples of rational functions with coefficients in \(\mathbb{C}\). Here \(\mathbf{x}, \boldsymbol{\mu}, \mathbf{y}, \mathbf{u}\) are states, parameters, outputs, and inputs, respectively.\pause{}
		\item Relies on Gr\"obner basis computation, where local and global identifiability is determined by the number of solutions from the basis.\pause{} %Based on the content of the basis, we conclude which parameters are globally identifiable (unique solution) and only locally identifiable.%, and which are not identifiable.
		\item User-fixed probability guarantee\footnote{{\sc Maple} guarantees correctness of Gr\"obner basis with probability \(1-10^{-18}\)} \(p\).
	\end{itemize}
\end{frame}
%\subsection{Step 2: Identifiable Combinations}
\begin{frame}[t]{Step 2: Identifiable Combinations}
	\begin{itemize}
		\item Same input: \[\Sigma:=\begin{cases}
				      \mathbf{x}'=\mathbf{f}(\mathbf{x},\boldsymbol{\mu}, \mathbf{u}), \\
				      \mathbf{y}=\mathbf{g}(\mathbf{x}, \boldsymbol{\mu}, \mathbf{u})
			      \end{cases}\]\pause{}
		\item Relying on computing input-output equations, the app returns \emph{single-} and \emph{multi-}experiment identifiable combinations.
	\end{itemize}
\end{frame}
\section{Efficiency}
\begin{frame}{Efficiency of the Toolbox}
	\begin{itemize}
		\item Step 1 is fast given our choice of {\tt tdeg} variable ordering in the Gr\"obner basis computation in {\sc Maple}.\pause{}
		\item In step 2, we reduce computation overhead taking advantage of output of step 1.\pause{}
		\item If step 1 reports everything globally identifiable with probability \(p\), then we report these parameters as the identifiable combinations.\pause{}
		\item Otherwise, if bound of multi-experiment identifiability is 1, we skip single-experiment calculations.\pause{}

	\end{itemize}
\end{frame}
\begin{frame}{Efficiency of the Toolbox}
	\begin{table}[htbp] % h:here; t:top; b:bottom; p:page; default:ht
		\caption{CPU Times with bypass for some of the built-in examples}
		\centering
		\begin{tabular}{lccc}
			\hline
			Name                            & Step 1 & Step 2, ME\footnote{multi-experiment identifiability} & Step 2, SE\footnote{single-experiment Identifiability} \\
			\hline
			{\tt Biohydrogenation}          & 3.747  & 160.861                                               & 160.861                                                \\
			{\tt Treatment}                 & 2.668  & 1.497                                                 & 1.497                                                  \\
			{\tt Tumor}                     & 6.784  & 0.004                                                 & 0.002                                                  \\
			{\tt Chemical Reaction Network} & 5.990  & 0.004\footnote{390.182 seconds without bypass}        & 0.002\footnotemark[8]
			\\
			% {\tt Chemical Reaction Network} (without bypass) & 4.906  & 390.182                                               & 390.182                                                \\
			\hline
		\end{tabular}
		\label{tab:def}
	\end{table}
\end{frame}

\section{Final Comments}
\begin{frame}[t]{Final Comments}

	\begin{itemize}
		\item It is now possible to run fast and efficient identifiability analysis for a variety of queries without requiring any software installed.\pause{}
		\item We greatly improved the runtime of Gr\"obner basis computation in our identifiability algorithm which allowed us to use output of step 1 and thus accelerate step 2.\pause{}
		\item One possible overhead that still remains to be resolved is computing characteristic sets of input-output equations via {\tt DifferentialAlgebra} (\cite{boulier2004blad}).\pause{}
		\item As a work in progress, we are working to incorporate Differential Thomas Decomposition for single- and multi-experiment identifiability problems~\cite{gerdt2019maple}.
	\end{itemize}

\end{frame}

\printbibliography{}
\begin{frame}
	\centering
	\Huge Thank you!
\end{frame}
\end{document}
